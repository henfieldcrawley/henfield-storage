Henfield Storage is family-owned and has been running for over 40 years. All of our personal and business customers benefit from low cost storage at our bases in North and West London, South and Southwest London, Central London, Surrey and West Sussex.

Address: B1, 3 Metcalf Way, Crawley RH11 7SU, UK

Phone: +44 1293 565045

Website: https://www.henfieldstorage.co.uk
